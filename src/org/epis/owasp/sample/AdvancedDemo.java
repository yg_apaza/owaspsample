package org.epis.owasp.sample;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.epis.owasp.OWASPAlgorithm;
import org.epis.owasp.OWASPException;
import org.epis.owasp.SecuredMessage;

public class AdvancedDemo
{
    public static void main(String[] args)
    {
        try
        {
            String message = "Hello World";
            
            // Almacen de claves
            
            KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
            char[] password = "testpwd".toCharArray();
            FileInputStream fis = new FileInputStream("testkeystore.ks");
            ks.load(fis, password);
            fis.close();
            
            // Clave publica y privada del emisor
            
            char[] keypassword = "send123".toCharArray();
            Key myKey = ks.getKey("testsender", keypassword);
            PrivateKey prvKeySender = (PrivateKey) myKey;
            
            X509Certificate sendercert = (X509Certificate) ks.getCertificate("testsender");
            PublicKey pubKeySender = sendercert.getPublicKey();
            
            // Clave publica y privada del receptor
            
            char[] recvpassword = "recv123".toCharArray();
            Key recvKey = ks.getKey("testrecv", recvpassword);
            PrivateKey prvKeyReceiver = (PrivateKey) recvKey;
            
            X509Certificate recvcert = (X509Certificate) ks.getCertificate("testrecv");
            PublicKey pubKeyReceiver = recvcert.getPublicKey();
            
            // Empaquetado del mensaje
            
            SecuredMessage securedMessage = OWASPAlgorithm.packMessage(message, prvKeySender, pubKeyReceiver, "SHA-256", "SHA256withRSA");
            
            // Validaciones y desencriptacion
            
            boolean signatureValidation = OWASPAlgorithm.validateSignature(securedMessage, pubKeySender, "SHA256withRSA");
            String decryptedmessage = OWASPAlgorithm.unpackMessage(securedMessage, prvKeyReceiver);
            boolean hashingValidation = OWASPAlgorithm.validateHashing(securedMessage, decryptedmessage, "SHA-256");
            
            System.out.println("OWASP Algorithm - SHA256 and RSA");
            System.out.println("Signature validation: " + signatureValidation);
            System.out.println("Hashing validation: " + hashingValidation);
            System.out.println("Decrypted message: " + decryptedmessage);
        }
        catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | UnrecoverableKeyException | OWASPException ex)
        {
            Logger.getLogger(SimpleDemo.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (FileNotFoundException ex)
        {
            Logger.getLogger(SimpleDemo.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (IOException ex)
        {
            Logger.getLogger(SimpleDemo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
